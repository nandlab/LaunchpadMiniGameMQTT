#ifndef LAUNCHPADMINIGAME_HPP
#define LAUNCHPADMINIGAME_HPP

#include "MQTTNode.hpp"
#include "LaunchpadMiniAsync.hpp"
#include <cstdint>
#include <fluidsynth.h>

class LaunchpadMiniGame : public MQTTNode
{
private:
    const unsigned midiInPort;
    const unsigned midiOutPort;
    LaunchpadMiniAsync launchpad;
    const std::string soundfont;
    fluid_settings_t * const fluidSettings;
    fluid_synth_t * const fluidSynth;
    fluid_audio_driver_t * const fluidAudioDriver;

    void launchpadCallback(std::uint8_t x, std::uint8_t y, std::uint8_t press);
    fluid_settings_t *createFluidSettings();
    fluid_synth_t *createFluidSynth();
    fluid_audio_driver_t *createFluidAudioDriver();
public:
    LaunchpadMiniGame(boost::asio::io_context &ioc,
                      const std::string &broker_hostname,
                      std::uint16_t broker_port,
                      const std::string &node_name,
                      unsigned midiInPort,
                      unsigned midiOutPort,
                      const std::string &soundfont);
    ~LaunchpadMiniGame();
    virtual void start() override;
};

#endif // LAUNCHPADMINIGAME_HPP
