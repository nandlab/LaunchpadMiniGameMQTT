#include <iostream>
#include "LaunchpadMiniGame.hpp"

int main()
{
    using std::cout;
    using std::endl;
    cout << "Hello World!" << endl;
    boost::asio::io_context ioc;
    cout << "IO context created." << endl;
    LaunchpadMiniGame game(ioc, "localhost", 1883, "launchpad", 0, 2, "/usr/share/sounds/sf2/FluidR3_GM.sf2");
    cout << "Game object created." << endl;
    game.start();
    cout << "Game started." << endl;
    ioc.run();
    cout << "Returned from IO context." << endl;
    return 0;
}
